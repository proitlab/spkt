BEGIN;
--
-- Create model LaporanKehilangan
--
CREATE TABLE "LaporanKehilangan_laporankehilangan" ("id" serial NOT NULL PRIMARY KEY, "uuid" uuid NOT NULL UNIQUE, "nama" varchar(50) NOT NULL, "tempat_lahir" varchar(20) NOT NULL, "tanggal_lahir" date NOT NULL, "alamat" varchar(200) NOT NULL, "kota" varchar(20) NOT NULL, "nomor_telpon" varchar(20) NOT NULL, "email" varchar(254) NOT NULL, "jenis_barang_hilang" varchar(100) NOT NULL, "tanggal_kehilangan" date NOT NULL, "lokasi_kehilangan" varchar(50) NOT NULL, "keterangan_kehilangan" text NOT NULL, "created_at" timestamp with time zone NOT NULL, "updated_at" timestamp with time zone NOT NULL, "hash_field" integer NOT NULL CHECK ("hash_field" >= 0), "is_valid" boolean NOT NULL, "is_approved" boolean NOT NULL, "is_expired" boolean NOT NULL, "nomor_surat" varchar(50) NOT NULL, "waktu_pelaporan" timestamp with time zone NOT NULL, "lokasi_pelaporan" varchar(50) NOT NULL, "pembuat_laporan" varchar(30) NOT NULL, "pangkat_pembuat_laporan" varchar(50) NOT NULL, "agama_id" integer NOT NULL, "created_by_id" integer NOT NULL, "jabatan_id" integer NOT NULL, "kebangsaan_id" integer NOT NULL, "kelamin_id" integer NOT NULL, "pekerjaan_id" integer NOT NULL);
ALTER TABLE "LaporanKehilangan_laporankehilangan" ADD CONSTRAINT "LaporanKehilangan_la_agama_id_b7574238_fk_InitData_" FOREIGN KEY ("agama_id") REFERENCES "InitData_agama" ("id") DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "LaporanKehilangan_laporankehilangan" ADD CONSTRAINT "LaporanKehilangan_la_created_by_id_a1175a21_fk_auth_user" FOREIGN KEY ("created_by_id") REFERENCES "auth_user" ("id") DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "LaporanKehilangan_laporankehilangan" ADD CONSTRAINT "LaporanKehilangan_la_jabatan_id_8098575b_fk_InitData_" FOREIGN KEY ("jabatan_id") REFERENCES "InitData_kepalasiaga" ("id") DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "LaporanKehilangan_laporankehilangan" ADD CONSTRAINT "LaporanKehilangan_la_kebangsaan_id_7c5622ef_fk_InitData_" FOREIGN KEY ("kebangsaan_id") REFERENCES "InitData_kebangsaan" ("id") DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "LaporanKehilangan_laporankehilangan" ADD CONSTRAINT "LaporanKehilangan_la_kelamin_id_673d92cc_fk_InitData_" FOREIGN KEY ("kelamin_id") REFERENCES "InitData_kelamin" ("id") DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE "LaporanKehilangan_laporankehilangan" ADD CONSTRAINT "LaporanKehilangan_la_pekerjaan_id_2ba21dc9_fk_InitData_" FOREIGN KEY ("pekerjaan_id") REFERENCES "InitData_pekerjaan" ("id") DEFERRABLE INITIALLY DEFERRED;
CREATE INDEX "LaporanKehilangan_laporankehilangan_agama_id_b7574238" ON "LaporanKehilangan_laporankehilangan" ("agama_id");
CREATE INDEX "LaporanKehilangan_laporankehilangan_created_by_id_a1175a21" ON "LaporanKehilangan_laporankehilangan" ("created_by_id");
CREATE INDEX "LaporanKehilangan_laporankehilangan_jabatan_id_8098575b" ON "LaporanKehilangan_laporankehilangan" ("jabatan_id");
CREATE INDEX "LaporanKehilangan_laporankehilangan_kebangsaan_id_7c5622ef" ON "LaporanKehilangan_laporankehilangan" ("kebangsaan_id");
CREATE INDEX "LaporanKehilangan_laporankehilangan_kelamin_id_673d92cc" ON "LaporanKehilangan_laporankehilangan" ("kelamin_id");
CREATE INDEX "LaporanKehilangan_laporankehilangan_pekerjaan_id_2ba21dc9" ON "LaporanKehilangan_laporankehilangan" ("pekerjaan_id");
COMMIT;
