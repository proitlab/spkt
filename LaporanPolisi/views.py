from django.shortcuts import render
from django.conf import settings
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic import DetailView, TemplateView
from django.urls import reverse_lazy, reverse
from django.core.exceptions import ObjectDoesNotExist
# from django.core.exceptions import EmptyResultSet
from django.http import Http404
from django.shortcuts import get_object_or_404
# from templated_email import send_templated_mail
from mail_templated import send_mail
from mail_templated import EmailMessage
from LaporanPolisi.forms import LaporanPolisiForm
from LaporanPolisi.models import LaporanPolisi


class Index(TemplateView):
    template_name = 'LaporanPolisi/index.html'

class LaporanPolisiCreate(CreateView):
    model = LaporanPolisi
    form_class = LaporanPolisiForm
    template_name = 'LaporanPolisi/add_form.html'
    # success_url = reverse_lazy('laporankasususer-detail')
    # fields = '__all__'

"""
class LaporanKasusUserUpdate(UpdateView):
    model = LaporanKasusUser
    form_class = LaporanKasusUserForm
    template_name = 'LaporanKasusUser/user_edit_form.html'
    # success_url = reverse_lazy('laporankasususer-detail')

    # This is to prevent direct call the link without referer
    def get(self, request, *args, **kwargs):
        if self.request.META.get('HTTP_REFERER') or settings.DEBUG:
            # print(self.request.META.get('HTTP_REFERER'))
            self.object = self.get_object()
            context = super(LaporanKasusUserUpdate,
                            self).get_context_data(*args, **kwargs)
            return self.render_to_response(context=context)
        else:
            raise Http404("Record is not found!")


class LaporanKasusUserDetail(DetailView):
    model = LaporanKasusUser
    # form_class = LaporanKehilanganUserForm
    template_name = 'LaporanKasusUser/user_detail_form.html'

    # This is to prevent direct call the link without referer
    def get(self, request, *args, **kwargs):
        if self.request.META.get('HTTP_REFERER') or settings.DEBUG:
            print(self.request.META.get('HTTP_REFERER'))
            self.object = self.get_object()
            context = super(LaporanKasusUserDetail,
                            self).get_context_data(*args, **kwargs)
            return self.render_to_response(context=context)
        else:
            raise Http404("Record is not found!")

class LaporanKasusUserStatus(DetailView):
    model = LaporanKasusUser
    # form_class = LaporanKehilanganUserForm
    template_name = 'LaporanKasusUser/user_status_form.html'


class LaporanKasusUserDelete(DeleteView):
    model = LaporanKasusUser
    success_url = reverse_lazy('landingpage-cancel')


# def QRCodeGenerator(request, pk, hf):
# def LaporanKehilanganUserConfirm(request, pk, hf):
def LaporanKasusUserConfirm(request, pk):
    if request.META.get('HTTP_REFERER') or settings.DEBUG:
        # print(request.META.get('HTTP_REFERER'))
        # qs = LaporanKehilanganUser.objects.filter(id=pk)
        # if qs.count() > 0:

        try:
            qs = LaporanKasusUser.objects.get(id=pk, is_valid=False)

            # send email here. Check if is_valid=True
            message = EmailMessage(
                'LaporanKasusUser/confirm.tpl', {
                    'DEBUG':
                    settings.DEBUG,
                    'object':
                    qs,
                    'request':
                    request,
                    'laporankehilanganuser_url':
                    request.build_absolute_uri(
                        reverse(
                            'laporankasususer:laporankasususer-status', kwargs={'pk': pk
                                                                    })),
                }, 'noreply@spkt.id', [qs.email])
            # do Something
            message.send()

            qs.is_valid = True
            qs.save()

        except ObjectDoesNotExist:
            raise Http404("Record is not found!")

        return render(request, 'LaporanKasusUser/user_confirm_form.html',
                      {
                          'pk': pk,
                          'hf': qs.hash_field,
                      })
        # else:
        #    raise Http404("Record is not found!")
    else:
        raise Http404("Record is not found!")


"""