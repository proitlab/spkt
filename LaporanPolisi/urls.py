from django.urls import path
# from django.conf import settings
# from django.conf.urls.static import static
from .views import Index
from .views import LaporanPolisiCreate
#from .views import LaporanKasusUserUpdate
#from .views import LaporanKasusUserDetail
#from .views import LaporanKasusUserStatus
#from .views import LaporanKasusUserDelete
#from .views import LaporanKasusUserConfirm


app_name = 'laporanpolisi'

urlpatterns = [
    path('', Index.as_view(), name='index'),
    path(
        'c/',
        LaporanPolisiCreate.as_view(),
        name='laporanpolisi-create'),
]
