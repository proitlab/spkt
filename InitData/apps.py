from django.apps import AppConfig


class InitdataConfig(AppConfig):
    name = 'InitData'
