from django.contrib import admin
from InitData.models import Kelamin, Agama, Pekerjaan, Kebangsaan, ModelLaporan
# from .models import LaporanKehilangan
from InitData.models import Lokasi, KepalaSiaga

admin.site.register(KepalaSiaga)
admin.site.register(Lokasi)
admin.site.register(Kelamin)
admin.site.register(Agama)
admin.site.register(Pekerjaan)
admin.site.register(Kebangsaan)
admin.site.register(ModelLaporan)
