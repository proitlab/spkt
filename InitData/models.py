from django.db import models


class KepalaSiaga(models.Model):
    jabatan = models.CharField(max_length=20)

    def __str__(self):
        return '%s' % (self.jabatan)


class Lokasi(models.Model):
    kode = models.CharField(max_length=20)
    deskripsi = models.CharField(max_length=100, blank=True)

    def __str__(self):
        return '%s' % (self.kode)


class Kelamin(models.Model):
    jenis_kelamin = models.CharField(max_length=50)

    def __str__(self):
        return '%s' % (self.jenis_kelamin)


class Agama(models.Model):
    jenis_agama = models.CharField(max_length=50)

    def __str__(self):
        return '%s' % (self.jenis_agama)


class Pekerjaan(models.Model):
    jenis_pekerjaan = models.CharField(max_length=50)

    def __str__(self):
        return '%s' % (self.jenis_pekerjaan)


class Kebangsaan(models.Model):
    jenis_kebangsaan = models.CharField(max_length=50)

    def __str__(self):
        return '%s' % (self.jenis_kebangsaan)

class ModelLaporan(models.Model):
    model_laporan = models.CharField(max_length=50)
    deskripsi = models.CharField(max_length=100, blank=True)

    def __str__(self):
        return '%s' % (self.model_laporan)

class Direktorat(models.Model):
    direktorat = models.CharField(max_length=50)
    deskripsi = models.CharField(max_length=100, blank=True)

    def __str__(self):
        return '%s' % (self.direktorat)