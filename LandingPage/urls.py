from django.urls import path
from .views import Index, Cancel


app_name = 'landingpage'

urlpatterns = [
    path(
        '',
        Index.as_view(),
        name='landingpage-index'),
    path(
        'batal/',
        Cancel.as_view(),
        name='landingpage-cancel'),
]
