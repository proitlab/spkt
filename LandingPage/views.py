# from django.shortcuts import render
from django.views.generic import TemplateView


class Index(TemplateView):
    template_name = 'LandingPage/index.html'


class Cancel(TemplateView):
    template_name = 'LandingPage/cancel.html'
