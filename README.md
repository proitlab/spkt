SPKT
====

Initial Setup
-------------

# Step untuk instalasi awal SPKT

#### Buat Database

Gunakan postgresql (dengan **docker** atau tidak).

    CREATE DATABASE spkt;
    CREATE USER spkt WITH PASSWORD 'salt****';
    GRANT ALL PRIVILEGES ON DATABASE spkt TO spkt;

#### Jalankan database migration dan buat user

    python manage.py migrate
    python manage.py createsuperuser

#### Inisialisasi Data

Jalankan script untuk loading data awal.

    ./initdata.sh



#### TextArea problem with line break

**NOTE:**
Pure CSS solution, not specific only to PHP
In normal cases, the TextArea HTML element is preserving the white space in the database.
The problem appears when trying to display the \n on the web browser, and this will fail.

To do that use the following code

    <p style="white-space: pre-line">multi-line text</p>


# Static Files URI

https://static.files.carisolusi.com/spkt/assets/images

It goes to docker nginx-static at server2.saltis.id