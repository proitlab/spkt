from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from InitData.models import Lokasi


class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    # lokasi = models.CharField(max_length=30, blank=True)
    lokasi = models.ForeignKey(
        Lokasi, default=1,
        on_delete=models.DO_NOTHING)

    @receiver(post_save, sender=User)
    def create_user_profile(sender, instance, created, **kwargs):
        if created:
            UserProfile.objects.create(user=instance)

    @receiver(post_save, sender=User)
    def save_user_profile(sender, instance, **kwargs):
        instance.userprofile.save()

    def __str__(self):
        return '%s %s' % (self.user, self.lokasi)
