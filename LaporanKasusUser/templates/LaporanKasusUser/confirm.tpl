{% extends "mail_templated/base.tpl" %}
{% load qr_code %}
{% block subject %}Laporan Kasus oleh {{ object.nama|upper }}{% endblock %}
{% block plain %}
Halo {{nama}},

Anda sudah melakukan Laporan Kasus pada {{ object.created_at|date:"d F Y H:m T" }}.
Link untuk status Laporan Anda disini:
{{ laporankasususer_url }}

Salam
{% endblock %}

{% block html %}
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/xhtml" style="font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif; margin: 0; padding: 0;">
<head>

<meta name="viewport" content="width=device-width" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>ZURBemails</title>

<style>img {
max-width: 100%;
}
body {
-webkit-font-smoothing: antialiased; -webkit-text-size-adjust: none; width: 100% !important; height: 100%;
}
@media only screen and (max-width: 600px) {
  a[class="btn"] {
    display: block !important; margin-bottom: 10px !important; background-image: none !important; margin-right: 0 !important;
  }
  div[class="column"] {
    width: auto !important; float: none !important;
  }
  table.social div[class="column"] {
    width: auto !important;
  }
}
</style></head>
<body bgcolor="#FFFFFF" style="font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif; -webkit-font-smoothing: antialiased; -webkit-text-size-adjust: none; width: 100% !important; height: 100%; margin: 0; padding: 0;">

<table class="head-wrap" bgcolor="#999999" style="font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif; width: 100%; margin: 0; padding: 0;">
<tr style="font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif; margin: 0; padding: 0;">
<td style="font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif; margin: 0; padding: 0;"></td>
<td class="header container" style="font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif; display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto; padding: 0;">
<div class="content" style="font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif; max-width: 600px; display: block; margin: 0 auto; padding: 15px;">
<table bgcolor="#999999" style="font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif; width: 100%; margin: 0; padding: 0;">
<tr style="font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif; margin: 0; padding: 0;">
<td style="font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif; margin: 0; padding: 0;"><img src="http://spkt.id/lambang_polda_metro_jaya.png" height="90" style="font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif; max-width: 100%; margin: 0; padding: 0;" /></td>
<td align="center" style="font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif; margin: 0; padding: 0;"><h1 class="collapse" style="font-family: &quot;HelveticaNeue-Light&quot;, &quot;Helvetica Neue Light&quot;, &quot;Helvetica Neue&quot;, Helvetica, Arial, &quot;Lucida Grande&quot;, sans-serif; line-height: 1.1; color: #333; font-weight: 900; font-size: 22px; text-transform: uppercase; margin: 0; padding: 0;">SENTRA PELAYANAN KEPOLISIAN TERPADU</h1></td>
</tr>
</table>
</div>
</td>
<td style="font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif; margin: 0; padding: 0;"></td>
</tr>
</table>

<table class="body-wrap" style="font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif; width: 100%; margin: 0; padding: 0;">
<tr style="font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif; margin: 0; padding: 0;">
<td style="font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif; margin: 0; padding: 0;"></td>
<td class="container" bgcolor="#FFFFFF" style="font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif; display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto; padding: 0;">
<div class="content" style="font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif; max-width: 600px; display: block; margin: 0 auto; padding: 15px;">
<table>
<tr>
<td>
<h3 style="font-family: &quot;HelveticaNeue-Light&quot;, &quot;Helvetica Neue Light&quot;, &quot;Helvetica Neue&quot;, Helvetica, Arial, &quot;Lucida Grande&quot;, sans-serif; line-height: 1.1; color: #000; font-weight: 500; font-size: 27px; margin: 0 0 15px; padding: 0;">Hi, {{ object.nama|upper }}</h3>
<p class="lead" style="font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif; font-weight: normal; font-size: 17px; line-height: 1.6; margin: 0 0 10px; padding: 0;">Anda telah berhasil membuat Laporan Kehilangan Anda pada {{ object.created_at|date:"d F Y H:m T" }}. Berikut adalah tiket laporan anda. Silahkan anda menuju kantor SPKT terdekat dan menunjukan e-tiket ini agar laporan anda dapat ditindaklanjuti.</p>
</td>
</tr>
<tr>
<td bgcolor="#ECF8FF">
<table><tr><td>
<p>
    Dengan ini, saya seorang
        {% if object.kelamin|upper != "PEREMPUAN" %}
        Laki-Laki
        {% else %}
        Perempuan
        {% endif %}
     Bangsa
        {% if object.kebangsaan|upper != "WNA" %}
        Indonesia
        {% else %}
        Asing
        {% endif %}
        yang mengaku bernama:
</p>
</td></tr></table>
<table>
    <tr>
        <td>Nama</td>
        <td>&nbsp;:&nbsp;</td>
        <td>{{ object.nama|upper }}</td>
    </tr>
    <!-- Tempat dan Tanggal Lahir -->
    <tr>
        <td>Tempat tanggal lahir</td>
        <td>&nbsp;:&nbsp;</td>
        <td>{{ object.tempat_lahir|upper }},
            {{ object.tanggal_lahir|date:"d F Y"|upper }}
        </td>
    </tr>
    <!-- Nomor Telpon -->
    <tr>
        <td>Nomor Telpon</td>
        <td>&nbsp;:&nbsp;</td>
        <td>{{ object.nomor_telpon }}</td>
    </tr>
    <!-- Email -->
    <tr>
        <td>Alamat Email</td>
        <td>&nbsp;:&nbsp;</td>
        <td>{{ object.email|upper }}</td>
    </tr>
    <!-- Agama -->
    <tr>
        <td>Agama</td>
        <td>&nbsp;:&nbsp;</td>
        <td>{{ object.agama|upper }}</td>
    </tr>
    <!-- Pekerjaan -->
    <tr>
        <td>Pekerjaan</td>
        <td>&nbsp;:&nbsp;</td>
        <td>{{ object.pekerjaan|upper }}</td>
    </tr>
    <!-- Kebangsaan -->
    <tr>
        <td>Kebangsaan</td>
        <td>&nbsp;:&nbsp;</td>
        <td>
            {% if object.kebangsaan != "WNA" %}
                INDONESIA
            {% else %}
                ASING
            {% endif %}

        </td>
    </tr>
    <!-- Alamat -->
    <tr>
        <td>Alamat</td>
        <td>&nbsp;:&nbsp;</td>
        <td><p style="white-space: pre-line">{{ object.alamat|upper }}</p></td>
    </tr>
    <tr>
        <td>
            &nbsp;
        </td>
        <td>
            &nbsp;
        </td>
        <td>{{ object.kota|upper }}</td>
    </tr>
</table>
<table><tr><td>
<p>
Melaporkan telah kehilangan surat / barang berupa:
</p>

<p style="white-space: pre-line">
{{ object.jenis_barang_hilang|upper }}
{% if object.keterangan_kehilangan.strip %}
        {{ object.keterangan_kehilangan|safe }}
{% endif %}
</p>

<p>
Terjadi pada tanggal {{ object.tanggal_kehilangan|date:"d F Y" }} di {{ object.lokasi_kehilangan }}. Kerugian atas nama
    dan alamat tersebut di atas.
</p>
</td>
</tr>
</table>
</td></tr></table>

<p class="callout" style="font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif; background-color: #FFFFFF; font-weight: normal; font-size: 14px; line-height: 1.6; margin: 0 0 15px; padding: 15px;">
Link untuk status laporan <a href="{{ laporankehilanganuser_url }}" style="font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif; color: #2BA6CB; font-weight: bold; margin: 0; padding: 0;">Klik Disini!</a>
</p>

<table align="center" class="social" width="100%" style="font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif; width: 100%; margin: 0; padding: 0;" bgcolor="#ebebeb">
<tr style="font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif; margin: 0; padding: 0;">
<td align="center" style="font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif; margin: 0; padding: 0;">

<table align="center" class="column" style="font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif; width: 280px; float: left; min-width: 279px; margin: 0; padding: 0;">
<tr style="font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif; margin: 0; padding: 0; vertical-align: middle;">
<td align="center" style="font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif; margin: 0; padding: 15px;">
<div class="content" style="font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif; max-width: 600px; display: block; margin: 0 auto; padding: 15px; text-align: center;">
<h5 class="" style="font-family: &quot;HelveticaNeue-Light&quot;, &quot;Helvetica Neue Light&quot;, &quot;Helvetica Neue&quot;, Helvetica, Arial, &quot;Lucida Grande&quot;, sans-serif; line-height: 1.1; color: #000; font-weight: 900; font-size: 25px; margin: 0 0 15px; padding: 0;">[QR-Code]</h5>
<h3 style="font-family: &quot;HelveticaNeue-Light&quot;, &quot;Helvetica Neue Light&quot;, &quot;Helvetica Neue&quot;, Helvetica, Arial, &quot;Lucida Grande&quot;, sans-serif; line-height: 1.1; color: #000; font-weight: 500; font-size: 36px; margin: 0 0 15px; padding: 0;">{{ object.hash_field }}</h3>
</div>
</td>
</tr>
</table>

<table align="center" class="column" style="font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif; width: 280px; float: left; min-width: 279px; margin: 0; padding: 0;">
<tr style="font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif; margin: 0; padding: 0; vertical-align: middle;">
<td align="center" style="font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif; margin: 0; padding: 15px;">
<div class="content" style="font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif; max-width: 600px; display: block; margin: 0 auto; padding: 15px; text-align: center;">
            {% if DEBUG %}
            <img src="http://server2.saltis.id:8000{% qr_url_from_text object.id|slugify size="T" version=1 image_format='png' %}"> 
            {% else %}
            <!-- Production with this -->
            <img src="{% if request.is_secure %}https://{% else %}http://{% endif %}{{ request.get_host }}{% qr_url_from_text object.id|slugify size="T" version=1 image_format='png' %}"> 
            {% endif %}
</div>
</td>
</tr>
</table>
<span class="clear" style="font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif; display: block; clear: both; margin: 0; padding: 0;"></span>
</td>
</tr>
</table>
</td>
</tr>
</table>
</div>
</td>
<td style="font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif; margin: 0; padding: 0;"></td>
</tr>
</table>

<table class="footer-wrap" style="font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif; width: 100%; clear: both !important; margin: 0; padding: 0;">
<tr style="font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif; margin: 0; padding: 0;">
<td style="font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif; margin: 0; padding: 0;"></td>
<td class="container" style="font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif; display: block !important; max-width: 600px !important; clear: both !important; margin: 0 auto; padding: 0;">

<div class="content" style="font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif; max-width: 600px; display: block; margin: 0 auto; padding: 15px; text-align: center;">
<p style="font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif; font-weight: normal; font-size: 14px; line-height: 1.6; margin: 0 0 10px; padding: 0;">
Power by <a href="http://saltis.id" style="font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif; color: #2BA6CB; margin: 0; padding: 0;">Saltis</a>
</p>
</div>
</td>
<td style="font-family: &quot;Helvetica Neue&quot;, &quot;Helvetica&quot;, Helvetica, Arial, sans-serif; margin: 0; padding: 0;"></td>
</tr>
</table>
</body>
</html>
{% endblock %}