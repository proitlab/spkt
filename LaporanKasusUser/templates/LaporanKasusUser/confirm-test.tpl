{% extends "mail_templated/base.tpl" %}
{% load qr_code %}
{% block subject %}Laporan Kehilangan oleh {{nama}}{% endblock %}
{% block plain %}
Halo {{nama}},

Anda sudah melakukan Laporan Kehilangan pada {{ created_at|date:"d F Y H:m T" }}.
Link untuk status Laporan Anda disini:
{{ laporankehilanganuser_url }}

Salam
{% endblock %}

{% block html %}
<p>Halo {{nama}},</p>

<p>Anda sudah melakukan Laporan Kehilangan pada {{ created_at|date:"d F Y H:m T" }}.</p>
<p>Link untuk status Laporan Anda <a href="{{ laporankehilanganuser_url }}">klik disini</a></p>

<img src="http://spkt.id/qr.jpg">
<img src="{% if request.is_secure %}https://{% else %}http://{% endif %}{{ request.get_host }}{% qr_url_from_text "{{ pk }}" size="M" version=10 image_format='png' %}">


<p>Salam</p>
<i>Admin</i>
{% endblock %}