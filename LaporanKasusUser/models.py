import hashlib
import uuid
from django.db import models
from django.urls import reverse
from django.utils.text import slugify
from django.core.exceptions import ValidationError
from InitData.models import Kelamin, Agama, Pekerjaan, Kebangsaan
from core.models import UpperCaseCharField, UpperCaseEmailField

# from django.utils import timezone
# from tinymce import models as tinymce_models


class LaporanKasusUser(models.Model):
    # Main Fields
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    nama = UpperCaseCharField(max_length=50)
    tempat_lahir = UpperCaseCharField(max_length=20, default=None)
    tanggal_lahir = models.DateField(
        auto_now=False, auto_now_add=False, default=None)
    kelamin = models.ForeignKey(
        Kelamin, default=1, on_delete=models.DO_NOTHING)
    agama = models.ForeignKey(Agama, default=1, on_delete=models.DO_NOTHING)
    pekerjaan = models.ForeignKey(
        Pekerjaan, default=1, on_delete=models.DO_NOTHING)
    kebangsaan = models.ForeignKey(
        Kebangsaan, default=1, on_delete=models.DO_NOTHING)
    # alamat = models.CharField(max_length=200)
    alamat = UpperCaseCharField(max_length=200)
    kota = UpperCaseCharField(max_length=20)
    nomor_telpon = UpperCaseCharField(max_length=20)
    email = UpperCaseEmailField(blank=True)

    # Kasus Field
    terlapor = UpperCaseCharField(max_length=50)
    korban = UpperCaseCharField(max_length=50)
    perkara = UpperCaseCharField(max_length=50)
    saksi = UpperCaseCharField(max_length=100)
    kerugian = UpperCaseCharField(max_length=200)
    waktu_kejadian = models.DateTimeField(
        auto_now=False, auto_now_add=False, default=None)
    tempat_kejadian = UpperCaseCharField(max_length=200)

    # Additional Fields for control
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    hash_field = models.PositiveIntegerField(default=0)
    is_valid = models.BooleanField(default=False)
    is_approved = models.BooleanField(default=False)
    is_expired = models.BooleanField(default=False)

    # keterangan_kehilangan = tinymce_models.HTMLField(blank=True)

    def __str__(self):
        return '%s' % (self.id)

    def get_absolute_url(self):
        return reverse(
            'laporankasususer:laporankasususer-detail', kwargs={'pk': self.pk})

    # def clean(self, *args, **kwargs):
    def clean(self):
        # Create hash string
        hash_string = "%s%s%s%s%s%s%s%s" % (
            slugify(self.nama),
            slugify(self.tempat_lahir),
            slugify(str(self.tanggal_lahir)),
            slugify(self.nomor_telpon),
            slugify(self.email),
            # Specific Kejadian Kasus Fields
            slugify(str(self.waktu_kejadian)),
            slugify(self.terlapor),
            slugify(self.korban),
        )

        # Encode the hash string to number
        hash_field = int(
            hashlib.sha256(hash_string.encode('utf-8')).hexdigest(),
            16) % 10**9

        # Do some validation
        query_string = LaporanKasusUser.objects.filter(
            hash_field=hash_field, is_valid=True)
        # query_string = self.objects.filter(
        #    hash_field=hash_field, is_valid=True)

        if self.id:
            query_string = query_string.exclude(pk=self.id)

        if query_string.count() > 0:
            raise ValidationError({
                'nama':
                'Laporan yang mirip sudah ada di sistem kami. \
                Apakah Anda membuat pelaporan yang sama?'
            })

        return self.id

    def save(self, *args, **kwargs):
        self.full_clean()

        # print("hahah")

        # Create hash string
        hash_string = "%s%s%s%s%s%s%s%s" % (
            slugify(self.nama),
            slugify(self.tempat_lahir),
            slugify(str(self.tanggal_lahir)),
            slugify(self.nomor_telpon),
            slugify(self.email),
            # Specific Kejadian Kasus Fields
            slugify(str(self.waktu_kejadian)),
            slugify(self.terlapor),
            slugify(self.korban),
        )

        print(hash_string)

        # Encode the hash string to number 9 Digit number
        self.hash_field = int(
            hashlib.sha256(hash_string.encode('utf-8')).hexdigest(),
            16) % 10**9

        super(LaporanKasusUser, self).save(*args, **kwargs)
