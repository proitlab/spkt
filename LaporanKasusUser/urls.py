from django.urls import path
# from django.conf import settings
# from django.conf.urls.static import static
from .views import LaporanKasusUserCreate
from .views import LaporanKasusUserUpdate
from .views import LaporanKasusUserDetail
from .views import LaporanKasusUserStatus
from .views import LaporanKasusUserDelete
from .views import LaporanKasusUserConfirm


app_name = 'laporankasususer'

urlpatterns = [
    path(
        'c/',
        LaporanKasusUserCreate.as_view(),
        name='laporankasususer-create'),
    path(
        'u/<uuid:pk>/',
        LaporanKasusUserUpdate.as_view(),
        name='laporankasususer-update'),
    path(
        'd/<uuid:pk>/',
        LaporanKasusUserDetail.as_view(),
        name='laporankasususer-detail'),
    path(
        's/<uuid:pk>/',
        LaporanKasusUserStatus.as_view(),
        name='laporankasususer-status'),
    path(
        'x/<uuid:pk>/',
        LaporanKasusUserDelete.as_view(),
        name='laporankasususer-delete'),
    path(
        # 'q/<uuid:pk>/<int:hf>/',
        'q/<uuid:pk>/',
        LaporanKasusUserConfirm,
        name='laporankasususer-confirm'),
]
