$(function() {
  $( "#id_tanggal_lahir" ).datepicker({
      dateFormat: 'dd-mm-yy',
      yearRange: "-100:+0", });
});

$(function() {
  $( "#id_tanggal_kehilangan" ).datepicker({
      dateFormat: 'dd-mm-yy', });
});

$(function() {
  $( "#id_waktu_kejadian" ).datetimepicker({
    dateFormat: 'yy-mm-dd HH:MM',
    controlType: 'select',
  });
});


new Vue({
  el : '#pernyataan',
  data: {
    checked: false
  }
});
