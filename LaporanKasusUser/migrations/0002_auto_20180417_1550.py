# Generated by Django 2.0.4 on 2018-04-17 08:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('LaporanKasusUser', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='laporankasususer',
            name='alamat',
            field=models.CharField(max_length=200),
        ),
        migrations.AlterField(
            model_name='laporankasususer',
            name='kerugian',
            field=models.CharField(max_length=200),
        ),
        migrations.AlterField(
            model_name='laporankasususer',
            name='tempat_kejadian',
            field=models.CharField(max_length=200),
        ),
    ]
