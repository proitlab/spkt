from django.urls import path
# from django.conf import settings
# from django.conf.urls.static import static
from .views import LaporanKehilanganUserCreate
from .views import LaporanKehilanganUserUpdate
from .views import LaporanKehilanganUserDetail
from .views import LaporanKehilanganUserStatus
from .views import LaporanKehilanganUserDelete
from .views import LaporanKehilanganUserConfirm

app_name = 'laporankehilanganuser'

urlpatterns = [
    path(
        'c/',
        LaporanKehilanganUserCreate.as_view(),
        name='laporankehilanganuser-create'),
    path(
        'u/<uuid:pk>/',
        LaporanKehilanganUserUpdate.as_view(),
        name='laporankehilanganuser-update'),
    path(
        'd/<uuid:pk>/',
        LaporanKehilanganUserDetail.as_view(),
        name='laporankehilanganuser-detail'),
    path(
        's/<uuid:pk>/',
        LaporanKehilanganUserStatus.as_view(),
        name='laporankehilanganuser-status'),
    path(
        'x/<uuid:pk>/',
        LaporanKehilanganUserDelete.as_view(),
        name='laporankehilanganuser-delete'),
    path(
        # 'q/<uuid:pk>/<int:hf>/',
        'q/<uuid:pk>/',
        LaporanKehilanganUserConfirm,
        name='laporankehilanganuser-confirm'),
]
