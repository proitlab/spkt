from django import forms
from django.forms import widgets
from LaporanKehilanganUser.models import LaporanKehilanganUser

# from django.forms.extras.widgets import SelectDateWidget
# from tinymce.widgets import TinyMCE
# from django.utils.translation import gettext_lazy as _


class LaporanKehilanganUserForm(forms.ModelForm):
    class Meta:
        model = LaporanKehilanganUser
        localized_fiels = '__all__'
        fields = [
            'nama',
            'tempat_lahir',
            'tanggal_lahir',
            'alamat',
            'kota',
            'kelamin',
            'agama',
            'pekerjaan',
            'kebangsaan',
            'nomor_telpon',
            'email',
            'jenis_barang_hilang',
            'tanggal_kehilangan',
            'lokasi_kehilangan',
            'keterangan_kehilangan',
        ]

        widgets = {
            # 'tanggal_lahir': SelectDateWidget(year=range(210, 1920, -1)),
            'nama':
            widgets.TextInput(attrs={
                'placeholder': 'Ketik Nama Lengkap',
                'class': 'form-control'
            }),
            'tempat_lahir':
            widgets.TextInput(attrs={
                'placeholder': 'Kota Tempat Anda Lahir',
                'class': 'form-control'
            }),
            'tanggal_lahir':
            forms.DateInput(
                format='%d-%m-%Y',
                attrs={
                    'class': 'datepicker form-control',
                    'placeholder': 'dd-mm-yyyy',
                    # widgets.DateInput(attrs={
                    #    'type': 'date',
                    #    'class': 'form-control',
                }),
            'tanggal_kehilangan':
            forms.DateInput(
                format='%d-%m-%Y',
                attrs={
                    'class': 'datepicker form-control',
                    'placeholder': 'dd-mm-yyyy',
                    # widgets.DateInput(attrs={
                    #    'type': 'date',
                    #    'class': 'form-control',
                }),
            'alamat':
            widgets.TextInput(attrs={
                'placeholder': 'Alamat tinggal Anda',
                'class': 'form-control',
            }),
            'kota':
            widgets.TextInput(attrs={
                'placeholder': 'Kota',
                'class': 'form-control',
            }),
            'nomor_telpon':
            widgets.TextInput(
                attrs={
                    'placeholder': 'Nomor Seluler Anda. Contoh: 08119998888',
                    'class': 'form-control',
                }),
            'jenis_barang_hilang':
            widgets.TextInput(
                attrs={
                    'placeholder': 'Contoh: STNK / SIM / MOBIL / ISTRI',
                    'class': 'form-control',
                }),
            'lokasi_kehilangan':
            widgets.TextInput(
                attrs={
                    'placeholder': 'Contoh: KANTOR / Sekitar Jalan Sudirman',
                    'class': 'form-control',
                }),
            'email':
            widgets.EmailInput(
                attrs={
                    'placeholder': 'Pastikan email Anda benar dan masih aktif',
                    'class': 'form-control',
                }),
            'kelamin':
            widgets.Select(attrs={
                'class': 'custom-select',
            }),
            'agama':
            widgets.Select(attrs={
                'class': 'custom-select',
            }),
            'pekerjaan':
            widgets.Select(attrs={
                'class': 'custom-select',
            }),
            'kebangsaan':
            widgets.Select(attrs={
                'class': 'custom-select',
            }),
            # 'tanggal_kehilangan': SelectDateWidget(),
            'keterangan_kehilangan':
            widgets.Textarea(
                attrs={
                    'class':
                    'form-control',
                    'placeholder':
                    'Tambahan keterangan mengenai barang yang hilang (jika ada).',
                }),
            # TinyMCE(attrs={
            #    'cols': 200,
            #    'rows': 80,
        }

        # help_texts = {
        # 'email': _('Pastikan email Anda benar dan masih aktif.'),
        # 'jenis_barang_hilang': _('Contoh: STNK, SIM, MOBIL.'),
        # 'keterangan_kehilangan':
        # _('Jabarkan detail kehilangan di kotak ini.'),
        # 'lokasi_kehilangan': _('Contoh: KANTOR, Sekitar Jalan Sudirman.'),
        # }

        # error_messages = {
        #    'email': _('Masukkan alamat email yang benar.'),
        # }
