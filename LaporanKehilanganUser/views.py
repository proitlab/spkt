from django.shortcuts import render
from django.conf import settings
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic import DetailView
from django.urls import reverse_lazy, reverse
from django.core.exceptions import ObjectDoesNotExist
# from django.core.exceptions import EmptyResultSet
from django.http import Http404
from django.shortcuts import get_object_or_404
# from templated_email import send_templated_mail
from mail_templated import send_mail
from mail_templated import EmailMessage
from LaporanKehilanganUser.forms import LaporanKehilanganUserForm
from LaporanKehilanganUser.models import LaporanKehilanganUser


class LaporanKehilanganUserCreate(CreateView):
    model = LaporanKehilanganUser
    form_class = LaporanKehilanganUserForm
    template_name = 'LaporanKehilanganUser/user_add_form.html'
    # success_url = reverse_lazy('laporankehilanganuser-detail')


class LaporanKehilanganUserUpdate(UpdateView):
    model = LaporanKehilanganUser
    form_class = LaporanKehilanganUserForm
    template_name = 'LaporanKehilanganUser/user_edit_form.html'
    # success_url = reverse_lazy('laporankehilanganuser-detail')

    # This is to prevent direct call the link without referer
    def get(self, request, *args, **kwargs):
        if self.request.META.get('HTTP_REFERER'):
            # print(self.request.META.get('HTTP_REFERER'))
            self.object = self.get_object()
            context = super(LaporanKehilanganUserUpdate,
                            self).get_context_data(*args, **kwargs)
            return self.render_to_response(context=context)
        else:
            raise Http404("Record is not found!")


class LaporanKehilanganUserDetail(DetailView):
    model = LaporanKehilanganUser
    # form_class = LaporanKehilanganUserForm
    template_name = 'LaporanKehilanganUser/user_detail_form.html'

    # This is to prevent direct call the link without referer
    def get(self, request, *args, **kwargs):
        if self.request.META.get('HTTP_REFERER') or settings.DEBUG:
            print(self.request.META.get('HTTP_REFERER'))
            self.object = self.get_object()
            context = super(LaporanKehilanganUserDetail,
                            self).get_context_data(*args, **kwargs)
            return self.render_to_response(context=context)
        else:
            raise Http404("Record is not found!")


class LaporanKehilanganUserStatus(DetailView):
    model = LaporanKehilanganUser
    # form_class = LaporanKehilanganUserForm
    template_name = 'LaporanKehilanganUser/user_status_form.html'


class LaporanKehilanganUserDelete(DeleteView):
    model = LaporanKehilanganUser
    success_url = reverse_lazy('landingpage:landingpage-cancel')


# def QRCodeGenerator(request, pk, hf):
# def LaporanKehilanganUserConfirm(request, pk, hf):
def LaporanKehilanganUserConfirm(request, pk):
    if request.META.get('HTTP_REFERER') or settings.DEBUG:
        # print(request.META.get('HTTP_REFERER'))
        # qs = LaporanKehilanganUser.objects.filter(id=pk)
        # if qs.count() > 0:

        try:
            qs = LaporanKehilanganUser.objects.get(id=pk, is_valid=False)

            # send email here. Check if is_valid=True
            message = EmailMessage(
                'LaporanKehilanganUser/confirm.tpl', {
                    'DEBUG':
                    settings.DEBUG,
                    'object':
                    qs,
                    'request':
                    request,
                    'laporankehilanganuser_url':
                    request.build_absolute_uri(
                        reverse(
                            'laporankehilanganuser:laporankehilanganuser-status', kwargs={'pk': pk
                                                                    })),
                }, 'noreply@spkt.id', [qs.email])
            # do Something
            message.send()

            qs.is_valid = True
            qs.save()

        except ObjectDoesNotExist:
            raise Http404("Record is not found!")

        return render(request, 'LaporanKehilanganUser/user_confirm_form.html',
                      {
                          'pk': pk,
                          'hf': qs.hash_field,
                      })
        # else:
        #    raise Http404("Record is not found!")
    else:
        raise Http404("Record is not found!")
