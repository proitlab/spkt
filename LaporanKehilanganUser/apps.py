from django.apps import AppConfig


class LaporankehilanganuserConfig(AppConfig):
    name = 'LaporanKehilanganUser'
