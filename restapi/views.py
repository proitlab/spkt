from rest_framework import viewsets
from LaporanKehilanganUser.models import LaporanKehilanganUser
from LaporanKehilangan.models import LaporanKehilangan
from LaporanKasusUser.models import LaporanKasusUser
# from LaporanKasus.models import LaporanKasus
from InitData.models import KepalaSiaga, Lokasi, Kelamin, Agama, Kebangsaan, Kelamin, Pekerjaan
from InitUser.models import UserProfile
from restapi.serializers import LaporanKehilanganUserSerializer, LaporanKasusUserSerializer
from restapi.serializers import LaporanKehilanganSerializer
from restapi.serializers import UserProfileSerializer
from restapi.serializers import KepalaSiagaSerilizer, LokasiSerializer, KelaminSerializer
from restapi.serializers import AgamaSerializer, KebangsaanSerializer, PekerjaanSerializer


class LaporanKehilanganUserViewset(viewsets.ModelViewSet):
    queryset = LaporanKehilanganUser.objects.all()
    serializer_class = LaporanKehilanganUserSerializer

class LaporanKasusUserViewset(viewsets.ModelViewSet):
    queryset = LaporanKasusUser.objects.all()
    serializer_class = LaporanKasusUserSerializer

class LaporanKehilanganViewset(viewsets.ModelViewSet):
    queryset = LaporanKehilangan.objects.all()
    serializer_class = LaporanKehilanganSerializer

class UserProfileViewset(viewsets.ModelViewSet):
    queryset = UserProfile.objects.all()
    serializer_class = UserProfileSerializer