from rest_framework import serializers
from LaporanKehilanganUser.models import LaporanKehilanganUser
from LaporanKehilangan.models import LaporanKehilangan
from LaporanKasusUser.models import LaporanKasusUser
# from LaporanKasus.models import LaporanKasus
from InitData.models import KepalaSiaga, Lokasi, Kelamin, Agama, Kebangsaan, Kelamin, Pekerjaan
from InitUser.models import UserProfile

# LaporanKehilanganUser
class LaporanKehilanganUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = LaporanKehilanganUser
        fields = '__all__'


class LaporanKehilanganSerializer(serializers.ModelSerializer):
    class Meta:
        model = LaporanKehilangan
        fields = '__all__'

# LaporanKasus
class LaporanKasusUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = LaporanKasusUser
        fields = '__all__'

'''
class LaporanKasusSerializer(serializers.ModelSerializer):
    class Meta:
        model = LaporanKasus
        fields = '__all__'
'''



# InitData
class KepalaSiagaSerilizer(serializers.ModelSerializer):
    class Meta:
        model = KepalaSiaga
        fields = '__all__'


class LokasiSerializer(serializers.ModelSerializer):
    class Meta:
        model = Lokasi
        fields = '__all__'


class KelaminSerializer(serializers.ModelSerializer):
    class Meta:
        model = Kelamin
        fields = '__all__'

class AgamaSerializer(serializers.ModelSerializer):
    class Meta:
        model = Agama
        fields = '__all__'

class PekerjaanSerializer(serializers.ModelSerializer):
    class Meta:
        model = Pekerjaan
        fields = '__all__'

class KebangsaanSerializer(serializers.ModelSerializer):
    class Meta:
        model = Kebangsaan
        fields = '__all__'

# InitUser
class UserProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserProfile
        fields = '__all__'
