from django.urls import path
from rest_framework.routers import DefaultRouter
from restapi.views import LaporanKehilanganUserViewset

router = DefaultRouter(trailing_slash=False)
router.register(prefix='laporankehilanganuser', viewset=LaporanKehilanganUserViewset)
# router.register(prefix='productcounter', viewset=ProductCounterViewSet)

# admin.site.site_header = 'Hokben Timer'
urlpatterns = router.urls
# urlpatterns += [
#    path('productcountersummary', getProductCounterSummary),
#    path('productcounterall', getProductCounterAll),
# ]