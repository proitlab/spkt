from django.contrib import admin
from django.urls import path, include
from qr_code import urls as qr_code_urls

urlpatterns = [
    path('', include('LandingPage.urls')),
    path('proadmin/', admin.site.urls),
    # path('lp/', include('LandingPage.urls')),
    path('feone/', include('LaporanKehilanganUser.urls')),
    path('fetwo/', include('LaporanKasusUser.urls')),
    path('beone/', include('LaporanKehilangan.urls')),
    path('betwo/', include('LaporanKasus.urls')),
    path('bethree/', include('LaporanPolisi.urls')),
    # path('tinymce/', include('tinymce.urls')),
    # path('laporankehilanganuser/', include('LaporanKehilanganUser.urls')),
    path('qr_code/', include(qr_code_urls, namespace="qr_code")),
    
    # rest api
    # path('theapi/', include('restapi.urls')),
]
