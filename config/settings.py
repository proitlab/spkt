import os
from decouple import config, Csv

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/2.0/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = config('SECRET_KEY')

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = config('DEBUG', default=True, cast=bool)

ALLOWED_HOSTS = config('ALLOWED_HOSTS', cast=Csv())

# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    # 'tinymce',
    'constance.backends.database',
    'constance',
    'rest_framework',
    'corsheaders',
    'bootstrap3',
    'qr_code',
    'mail_templated',
    # SPKT from here
    'restapi.apps.RestapiConfig',
    # 'restapi',
    'core.apps.CoreConfig',
    'InitData.apps.InitdataConfig',
    'InitUser.apps.InituserConfig',
    'LandingPage.apps.LandingpageConfig',
    'LaporanKehilangan.apps.LaporankehilanganConfig',
    'LaporanKehilanganUser.apps.LaporankehilanganuserConfig',
    'LaporanKasus.apps.LaporankasusConfig',
    'LaporanKasusUser.apps.LaporankasususerConfig',
    'LaporanPolisi.apps.LaporanpolisiConfig',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'config.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'constance.context_processors.config',
            ],
        },
    },
]


WSGI_APPLICATION = 'config.wsgi.application'

# Database
# https://docs.djangoproject.com/en/2.0/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': config('DB_NAME'),
        'USER': config('DB_USER'),
        'PASSWORD': config('DB_PASS'),
        'HOST': config('DB_HOST'),
        'PORT': config('DB_PORT'),
    }
}

# Password validation
# https://docs.djangoproject.com/en/2.0/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME':
        'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME':
        'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME':
        'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME':
        'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

# Internationalization
# https://docs.djangoproject.com/en/2.0/topics/i18n/

LANGUAGE_CODE = 'id-id'

TIME_ZONE = 'Asia/Jakarta'

USE_I18N = True

USE_L10N = True

USE_TZ = True

DATE_INPUT_FORMATS = ('%d-%m-%Y')

CONSTANCE_BACKEND = 'constance.backends.database.DatabaseBackend'
# CONSTANCE_BACKEND = 'constance.backends.redisd.RedisBackend'
CONSTANCE_REDIS_CONNECTION = config('CONSTANCE_REDIS_CONNECTION')

CONSTANCE_CONFIG = {
    'AWAL_NS_LAPORAN_KEHILANGAN': (7000, 'Variable awal Laporan Kehilangan'),
    'AWAL_NS_LAPORAN_KASUS': (8000, 'Variable awal Laporan Kasus'),
    'AWAL_NS_LAPORAN_POLISI': (9000, 'Variable awal Laporan Polisi'),
    'MASA_BERLAKU_NS': (14, 'Masa berlaku surat (dalam hari)'),
    'APPNAME': ('SPKT', 'Application Name'),
    'VERSION': ('5.0.0', 'Software Version'),
}

TINYMCE_DEFAULT_CONFIG = {
    'theme': "simple",  # default value
    'skin': "default",
    'relative_urls': False,  # default value
    'width': 500,
    'height': 200,
    'forced_root_block': False,
}

TINYMCE_COMPRESSOR = True

BOOTSTRAP3 = {
    'css_url': config('BOOTSTRAP_CSS_URL'),
    # 'theme_url': 'https://bootswatch.com/4/minty/bootstrap.min.css',
    'include_jquery': True,
}

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.0/howto/static-files/

STATIC_URL = config('STATIC_URL', default='/static/')
STATIC_ROOT = os.path.join(BASE_DIR, 'static/')

# Email Backend
EMAIL_HOST = config('EMAIL_HOST')
EMAIL_PORT = config('EMAIL_PORT')
EMAIL_HOST_USER = config('EMAIL_HOST_USER')
EMAIL_HOST_PASSWORD = config('EMAIL_HOST_PASSWORD')
EMAIL_USE_TLS = config('EMAIL_USE_TLS', default=True, cast=bool)


# QR
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
    },
    'qr-code': {
        'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
        'LOCATION': 'qr-code-cache',
        'TIMEOUT': 3600
    }
}

QR_CODE_CACHE_ALIAS = 'qr-code'

# DATE_INPUT_FORMATS = ['%d-%m-%Y']

# REDIS related settings 
REDIS_HOST = config('REDIS_HOST')
REDIS_PORT = config('REDIS_PORT')
BROKER_URL = 'redis://' + REDIS_HOST + ':' + REDIS_PORT + '/0'
BROKER_TRANSPORT_OPTIONS = {'visibility_timeout': 3600} 
CELERY_RESULT_BACKEND = 'redis://' + REDIS_HOST + ':' + REDIS_PORT + '/0'