from django.views.generic import DetailView, TemplateView, UpdateView
from django.conf import settings
from django.http import Http404
from django.utils import timezone
from django.shortcuts import render, redirect, reverse
from django.shortcuts import get_object_or_404
from django.core.exceptions import ObjectDoesNotExist
from LaporanKasusUser.models import LaporanKasusUser
from LaporanKasusUser.forms import LaporanKasusUserForm
from LaporanKasus.models import LaporanKasus
from LaporanPolisi.models import LaporanPolisi
from LaporanKasusUser.views import LaporanKasusUserDetail
from LaporanKasusUser.views import  LaporanKasusUserUpdate
from InitData.models import Agama, Kelamin, Kebangsaan, Pekerjaan, Lokasi, ModelLaporan, Direktorat
from core.templatetags.core_tags import nomorsuratLaporanKasus
from core.templatetags.core_filters import roman_number


class Index(TemplateView):
    template_name = 'LaporanKasus/index.html'


def getLaporanKasusUserDetail(request):
    template_name = "LaporanKasus/user_status_form.html"
    if request.POST and request.POST['qrcode']:
        qrcode = request.POST['qrcode']
        # if isinstance(qrcode, int):
        if qrcode.isdigit():
            try:
                qs = LaporanKasusUser.objects.get(
                    hash_field=qrcode, is_valid=True, is_expired=False)
                STATUS = True
            except ObjectDoesNotExist:
                raise Http404("Record is not found!")

            if qs.is_approved:
                try:
                    qs = LaporanKasus.objects.get(
                        hash_field=qrcode, is_valid=True, is_expired=False)
                    STATUS = False
                except ObjectDoesNotExist:
                    raise Http404("Record is not found or expired!")
        else:
            try:
                qs = LaporanKasusUser.objects.get(
                    id=qrcode, is_valid=True, is_expired=False)
                STATUS = True
            except ObjectDoesNotExist:
                raise Http404("Record is not found!")

            if qs.is_approved:
                try:
                    qs = LaporanKasus.objects.get(
                        id=qrcode, is_valid=True, is_expired=False)
                    STATUS = False
                except ObjectDoesNotExist:
                    raise Http404("Record is not found or expired!")


        if STATUS:
            return redirect('laporankasus:laporankasususer-detail', pk = qs.id)
        else:
            return redirect(reverse('laporankasus:laporankasus-print', args = {qs.id}))


    else:
        raise Http404("Record is not found!")


class LaporanKasusUserDetail(LaporanKasusUserDetail):
    model = LaporanKasusUser
    # form_class = LaporanKehilanganUserForm
    template_name = 'LaporanKasus/user_status_form.html'

# class LaporanKehilanganUserUpdate(UpdateView):
class LaporanKasusUserUpdate(LaporanKasusUserUpdate):
    model = LaporanKasusUser
    form_class = LaporanKasusUserForm
    template_name = 'LaporanKasus/user_edit_form.html'

    def get_success_url(self, *args, **kwargs):
        return reverse('laporankasus:laporankasususer-detail', args = {self.object.id})


# EDIT THIS FOR FURTHER PROCESSING
def LaporanKasusContinue(request, pk):
    if request.META.get('HTTP_REFERER'):
        # print(request.META.get('HTTP_REFERER'))
        # qs = LaporanKehilanganUser.objects.filter(id=pk)
        # if qs.count() > 0:

        direktorats = Direktorat.objects.all()

        try:
            qs = LaporanKasusUser.objects.get(id=pk, is_valid=True)

            qs.is_valid = True
            qs.save()

        except ObjectDoesNotExist:
            raise Http404("Record is not found!")
        
        lokasi = Lokasi.objects.all()

        return render(request, 'LaporanKasus/continue_process.html',
                      {
                          'pk': pk,
                          'hf': qs.hash_field,
                          'object': qs,
                          'lokasi': lokasi,
                          'direktorats': direktorats,
                      })
        # else:
        #    raise Http404("Record is not found!")
    else:
        raise Http404("Record is not found!")


        # EDIT THIS FOR FURTHER PROCESSING
def LaporanKasusSave(request):
    if request.META.get('HTTP_REFERER') or settings.DEBUG:

        # agama = request.POST.get('id_agama')
        # kelamin = request.POST.get('id_kelamin')
        # pekerjaan = request.POST.get('id_pekerjaan')
        # kebangsaan = request.POST.get('id_kebangsaan')
        laporankasususer_id = request.POST.get('id_id')
        modellaporan_user = ModelLaporan.objects.get(id=3) # Model - B1
        modellaporan_polisi = ModelLaporan.objects.get(id=2) # Model - B
        direktorat = Direktorat.objects.get(id=request.POST.get('id_direktorat'))
        
        # LaporanKasusUser
        qs_s = LaporanKasusUser.objects.get(id=laporankasususer_id)
        
        if not qs_s.is_approved:
            qs_s.is_approved = True
        

        # LaporanKasus       
        qs = LaporanKasus()
        qs_p = LaporanPolisi()

        # LaporanKasus - Foreignkey
        qs.agama = qs_s.agama
        qs.kelamin = qs_s.kelamin
        qs.pekerjaan = qs_s.pekerjaan
        qs.kebangsaan = qs_s.kebangsaan
        
        # LaporanPolisi - Foreignkey
        qs_p.agama = qs_s.agama
        qs_p.kelamin = qs_s.kelamin
        qs_p.pekerjaan = qs_s.pekerjaan
        qs_p.kebangsaan = qs_s.kebangsaan

        # LaporanKasus - Fields User
        qs.nama = qs_s.nama
        qs.tempat_lahir = qs_s.tempat_lahir
        qs.tanggal_lahir = qs_s.tanggal_lahir
        qs.alamat = qs_s.alamat
        qs.kota = qs_s.kota
        qs.nomor_telpon = qs_s.nomor_telpon
        qs.email = qs_s.email
        
        # LaporanPolisi - Fields User 
        qs_p.nama = qs_s.nama
        qs_p.tempat_lahir = qs_s.tempat_lahir
        qs_p.tanggal_lahir = qs_s.tanggal_lahir
        qs_p.alamat = qs_s.alamat
        qs_p.kota = qs_s.kota
        qs_p.nomor_telpon = qs_s.nomor_telpon
        qs_p.email = qs_s.email

        # LaporanKasus - Field Kasus
        qs.terlapor = qs_s.terlapor
        qs.korban = qs_s.korban
        qs.perkara = qs_s.perkara
        qs.saksi = qs_s.saksi
        qs.kerugian = qs_s.kerugian
        qs.waktu_kejadian = qs_s.waktu_kejadian
        qs.tempat_kejadian = qs_s.tempat_kejadian
        qs.uraian_singkat_kejadian = request.POST.get('id_uraian_singkat_kejadian')

        # LaporanPolisi - Field Kasus
        qs_p.terlapor = qs_s.terlapor
        qs_p.korban = qs_s.korban
        qs_p.perkara = qs_s.perkara
        qs_p.saksi = qs_s.saksi
        qs_p.kerugian = qs_s.kerugian
        qs_p.waktu_kejadian = qs_s.waktu_kejadian
        qs_p.tempat_kejadian = qs_s.tempat_kejadian
        qs_p.uraian_singkat_kejadian = request.POST.get('id_uraian_singkat_kejadian')

        # LaporanKasus - Field Back Office
        qs.jabatan = request.POST.get('id_jabatan')
        qs.pembuat_laporan = request.POST.get('id_pembuat_laporan')
        qs.pangkat_pembuat_laporan = request.POST.get('id_pangkat_pembuat_laporan')
        qs.lokasi_pelaporan = request.POST.get('id_lokasi_pelaporan')
        qs.pasal_perkara = request.POST.get('id_pasal_perkara')
        qs.direktorat = direktorat

        # LaporanPolisi - Field Back Office
        qs_p.jabatan = request.POST.get('id_jabatan')
        qs_p.pembuat_laporan = request.POST.get('id_pembuat_laporan')
        qs_p.pangkat_pembuat_laporan = request.POST.get('id_pangkat_pembuat_laporan')
        qs_p.lokasi_pelaporan = request.POST.get('id_lokasi_pelaporan')
        qs_p.pasal_perkara = request.POST.get('id_pasal_perkara')
        qs_p.direktorat = direktorat
        
        # LaporanKasus - Fields Control
        qs.uuid = qs_s.id
        qs.hash_field = qs_s.hash_field
        qs.is_valid = True
        qs.is_approved = True
        qs.model_laporan = modellaporan_user
        qs.model_laporan_polisi = modellaporan_polisi


        # LaporanPolisi - Fields Control
        qs_p.uuid = qs_s.id
        qs_p.hash_field = qs_s.hash_field
        qs_p.is_valid = True
        qs_p.is_approved = True
        qs_p.model_laporan = modellaporan_polisi

        # Nomor Surat
        qs.nomor_surat = nomorsuratLaporanKasus()
        qs.nomor_surat_text = '%s / %s / %s / %s / %s / %s' % (
            'TBL',
            qs.nomor_surat,
            roman_number(timezone.now().month),
            timezone.now().year,
            qs.lokasi_pelaporan,
            direktorat.direktorat,
            )

        # Nomor Surat
        qs_p.nomor_surat = nomorsuratLaporanKasus()
        qs_p.nomor_surat_text = '%s / %s / %s / %s / %s / %s' % (
            'TBL',
            qs.nomor_surat,
            roman_number(timezone.now().month),
            timezone.now().year,
            qs.lokasi_pelaporan,
            direktorat.direktorat,
            )
        
            
        # qs.save()

        # Save into LaporanKehilangan    
        try:
            qs_s.save()
            qs_p.save()
            qs.save()
        except Exception:
            # from django.db import connection
            # print(connection.queries[-1])
            # print ('%s (%s)' % (e.message, type(e)))
            raise Http404("Something Wrong! Can't Save LK!")
        
        # Update LaporanKehilanganUser. is_approved=True
        # id_uuid = request.POST.get('id_uuid')
        
        # try:
        #    qs_user = LaporanKasusUser.objects.get(id=id_uuid)
        # except ObjectDoesNotExist:
        #    raise Http404("Record is not found!")

        # qs_user.is_approved = True;
        # qs_user.is_valid = True;

        # Try to save into LaporanKehilanganUser
        # try:
        #    qs_user.save()
        # except Exception:
        #    raise Http404("Something Wrong! Can't Save!")

        # Get The Lastest record and 
        # try:
        #    qs = LaporanKehilangan.objects.get(uuid=id_uuid, is_valid=True, is_approved=True)

        # except ObjectDoesNotExist:
        #    raise Http404("Record is not found!")

        # return LaporanKehilanganPrint.as_view()(request, pk=qs.id)
        return redirect(reverse('laporankasus:laporankasus-print', args = {qs.id}))

    else:
        raise Http404("Record is not found!")


class LaporanKasusPrint(DetailView):
    model = LaporanKasus
    template_name = 'LaporanKasus/save_and_print.html'