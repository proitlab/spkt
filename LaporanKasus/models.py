import hashlib
import uuid
from django.contrib.auth.models import User
from django.db import models
from django.urls import reverse
from django.utils.text import slugify
from django.core.exceptions import ValidationError
from InitData.models import Kelamin, Agama, Pekerjaan, Kebangsaan, ModelLaporan, Direktorat
from core.models import UpperCaseCharField, UpperCaseCharWithoutSpaceField, UpperCaseEmailField

# from django.utils import timezone
# from tinymce import models as tinymce_models


class LaporanKasus(models.Model):
    # Main Fields
    uuid = models.UUIDField(unique=True, default=uuid.uuid4, editable=False)
    nama = UpperCaseCharField(max_length=50)
    tempat_lahir = UpperCaseCharField(max_length=20, default=None)
    tanggal_lahir = models.DateField(
        auto_now=False, auto_now_add=False, default=None)
    kelamin = models.ForeignKey(
        Kelamin, default=1, on_delete=models.DO_NOTHING)
    agama = models.ForeignKey(Agama, default=1, on_delete=models.DO_NOTHING)
    pekerjaan = models.ForeignKey(
        Pekerjaan, default=1, on_delete=models.DO_NOTHING)
    kebangsaan = models.ForeignKey(
        Kebangsaan, default=1, on_delete=models.DO_NOTHING)
    # alamat = models.CharField(max_length=200)
    alamat = UpperCaseCharField(max_length=200)
    kota = UpperCaseCharField(max_length=20)
    nomor_telpon = UpperCaseCharField(max_length=20)
    email = UpperCaseEmailField(blank=True)

    # Kasus Field
    terlapor = UpperCaseCharField(max_length=50)
    korban = UpperCaseCharField(max_length=50)
    perkara = UpperCaseCharField(max_length=50)
    saksi = UpperCaseCharField(max_length=100)
    kerugian = UpperCaseCharField(max_length=200)
    waktu_kejadian = models.DateTimeField(
        auto_now=False, auto_now_add=False, default=None)
    tempat_kejadian = UpperCaseCharField(max_length=200)
    uraian_singkat_kejadian = models.TextField(blank=True)

    # Nomor Surat
    nomor_surat = models.PositiveIntegerField(default=0)
    nomor_surat_polisi = models.PositiveIntegerField(default=0)
    nomor_surat_text = UpperCaseCharField(max_length=50, blank=True)
    nomor_surat_text_polisi = UpperCaseCharField(max_length=50, blank=True)
    waktu_pelaporan = models.DateTimeField(auto_now=False, auto_now_add=True)
    lokasi_pelaporan = UpperCaseCharWithoutSpaceField(max_length=50)

    # Kasus
    # tindak_pidana = models.CharField(max_lenght=50)
    pasal_perkara = UpperCaseCharField(max_length=50)
    model_laporan = models.ForeignKey(
        ModelLaporan, default=3, on_delete=models.DO_NOTHING)
    
    #model_laporan_polisi = models.ForeignKey(
    #     ModelLaporan, default=1, on_delete=models.DO_NOTHING)

    # Direktorat
    direktorat = models.ForeignKey(
        Direktorat, default=1, on_delete=models.DO_NOTHING)

    # Tanda Tangan
    jabatan = UpperCaseCharField(max_length=20)
    pembuat_laporan = UpperCaseCharField(max_length=30, blank=True)
    pangkat_pembuat_laporan = UpperCaseCharField(max_length=50, blank=True)

    # Additional Fields for control
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    hash_field = models.PositiveIntegerField(default=0)
    is_valid = models.BooleanField(default=False)
    is_approved = models.BooleanField(default=False)
    is_expired = models.BooleanField(default=False)

    created_by = models.ForeignKey(
        User, on_delete=models.DO_NOTHING, default=1)

    # keterangan_kehilangan = tinymce_models.HTMLField(blank=True)

    def __str__(self):
        return '%s' % (self.nomor_surat_text)


'''
    def get_absolute_url(self):
        return reverse(
            'laporankasus:laporankasus-detail', kwargs={'pk': self.pk})

'''
