from django.urls import path
from LaporanKasus.views import Index, getLaporanKasusUserDetail
from LaporanKasus.views import LaporanKasusUserDetail, LaporanKasusUserUpdate
from LaporanKasus.views import LaporanKasusContinue, LaporanKasusSave
from LaporanKasus.views import LaporanKasusPrint

app_name = 'laporankasus'
urlpatterns = [
    path('', Index.as_view(), name='index'),
    path(
        'g/',
        getLaporanKasusUserDetail,
        name='getlaporankasususer-detail'),
    path(
        'd/<uuid:pk>/',
        LaporanKasusUserDetail.as_view(),
        name='laporankasususer-detail'),
    path(
        'u/<uuid:pk>/',
        LaporanKasusUserUpdate.as_view(),
        name='laporankasususer-update'),
    path(
        # 'q/<uuid:pk>/<int:hf>/',
        'q/<uuid:pk>/',
        LaporanKasusContinue,
        name='laporankasus-continue'),
    path(
        'p/',
        LaporanKasusSave,
        name='laporankasus-save'),
    path(
        'print/<pk>/',
        LaporanKasusPrint.as_view(),
        name='laporankasus-print'),   
]