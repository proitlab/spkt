from django import forms
from django.forms import widgets
from .models import LaporanKasus


class LaporanKasusForm(forms.ModelForm):
    waktu_kejadian = forms.DateTimeField(
        input_formats=['%d-%m-%Y %H:%M'],
        widget=forms.DateTimeInput(
                format='%d-%m-%Y %H:%M',
                attrs={
                'class': 'form-control',
                'placeholder': 'dd-mm-yyyy HH:MM:ss',
            }))
    class Meta:
        model = LaporanKasus
        localized_fiels = '__all__'
        fields = [
            'nama',
            'tempat_lahir',
            'tanggal_lahir',
            'alamat',
            'kota',
            'kelamin',
            'agama',
            'pekerjaan',
            'kebangsaan',
            'nomor_telpon',
            'email',
            'waktu_kejadian',
            'tempat_kejadian',
            'terlapor',
            'korban',
            'perkara',
            'saksi',
            'kerugian',
        ]

        widgets = {
            'nama':
            widgets.TextInput(attrs={
                'placeholder': 'Ketik Nama Lengkap',
                'class': 'form-control',
            }),
            'tempat_lahir':
            widgets.TextInput(attrs={
                'placeholder': 'Kota Tempat Anda Lahir',
                'class': 'form-control',
            }),
            'tanggal_lahir':
            forms.DateInput(
                format='%d-%m-%Y',
                attrs={
                'class': 'datepicker form-control',
                'placeholder': 'dd-mm-yyyy',
            }),
            'alamat':
            widgets.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Alamat',
            }),
            'kota':
            widgets.TextInput(attrs={
                'placeholder': 'Kota',
                'class': 'form-control',
            }),
            'nomor_telpon':
            widgets.TextInput(
                attrs={
                    'placeholder': 'Nomor Seluler Anda. Contoh: 08119998888',
                    'class': 'form-control',
                }),
            'email':
            widgets.EmailInput(
                attrs={
                    'placeholder': 'Pastikan email Anda benar dan masih aktif',
                    'class': 'form-control',
                }),
            'kelamin':
            widgets.Select(attrs={
                'class': 'custom-select',
            }),
            'agama':
            widgets.Select(attrs={
                'class': 'custom-select',
            }),
            'pekerjaan':
            widgets.Select(attrs={
                'class': 'custom-select',
            }),
            'kebangsaan':
            widgets.Select(attrs={
                'class': 'custom-select',
            }),
            'waktu_kejadian':
            forms.DateTimeInput(
                attrs={
                'class': 'form-control',
                'placeholder': 'dd-mm-yyyy HH:MM:ss',
            }),
            'tempat_kejadian':
            widgets.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Tempat Kejadian',
            }),
            'terlapor':
            widgets.TextInput(attrs={
                'placeholder': 'Nama si pelaku',
                'class': 'form-control',
            }),
            'korban':
            widgets.TextInput(
                attrs={
                    'placeholder': 'Nama korban atau pelapor',
                    'class': 'form-control',
                }),
            'perkara':
            widgets.TextInput(
                attrs={
                    'placeholder': 'Penganiayaan, Pencurian, Perampokan',
                    'class': 'form-control',
                }),
            'saksi':
            widgets.TextInput(
                attrs={
                    'placeholder': 'Nama Saksi (Boleh lebih dari 1 orang)',
                    'class': 'form-control',
                }),
            'kerugian':
            widgets.TextInput(attrs={
                'class': 'form-control',
                'placeholder': 'Kerugian yang dialami',
            }),
        }

        # help_texts = {
        # 'email': _('Pastikan email Anda benar dan masih aktif.'),
        # 'jenis_barang_hilang': _('Contoh: STNK, SIM, MOBIL.'),
        # 'keterangan_kehilangan':
        # _('Jabarkan detail kehilangan di kotak ini.'),
        # 'lokasi_kehilangan': _('Contoh: KANTOR, Sekitar Jalan Sudirman.'),
        # }

        # error_messages = {
        #    'email': _('Masukkan alamat email yang benar.'),
        # }
