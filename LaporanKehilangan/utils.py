import datetime
from django.template.defaultfilters import date
from django.utils import timezone
from constance import config
from .models import LaporanKehilangan
from django.core.exceptions import ObjectDoesNotExist

numeral_map = tuple(
    zip((1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1),
        ('M', 'CM', 'D', 'CD', 'C', 'XC', 'L', 'XL', 'X', 'IX', 'V', 'IV',
         'I')))


def int_to_roman(i):
    result = []
    for integer, numeral in numeral_map:
        count = i // integer
        result.append(numeral * count)
        i -= integer * count
    return ''.join(result)


def roman_to_int(n):
    i = result = 0
    for integer, numeral in numeral_map:
        while n[i:i + len(numeral)] == numeral:
            result += integer
            i += len(numeral)
    return result


# def buat_nomorsurat():
def buat_nomorsurat(location):
    '''
    No. Surat (eg.7563/B/VI/2017/PMJ/SPKT)
    '''
    try:
        latest_record = LaporanKehilangan.objects.last()
        current_NS = latest_record.nomor_surat
        just_NS = current_NS.split(' / ')
        # Populate Nomor Surat
        # field_1 = config.AWAL_NS + NomorSurat.objects.count() + 1
        field_1 = int(just_NS[0]) + 1
    except ObjectDoesNotExist:
        field_1 = config.AWAL_NS

    today = datetime.date.today()
    # delimiter = '/'
    # Populate Nomor Surat
    # field_1 = config.AWAL_NS + NomorSurat.objects.count() + 1
    # field_1 = int(just_NS[0]) + 1
    # field_1 = i + 1     # Auto increment number
    field_2 = 'B'  # Always B
    field_3 = int_to_roman(int(date(today, 'n')))  # Month in Roman
    field_4 = date(today, 'Y')  # Year
    field_5 = 'PMJ'  # Always PMJ
    # field_6 = 'SPKT'    # Always SPKT
    field_6 = str(location)  # Always SPKT

    nomorsurat_baru = '{} / {} / {} / {} / {} / {}'.format(
        field_1, field_2, field_3, field_4, field_5, field_6)

    return nomorsurat_baru


def menghitung_hari(waktu_pelaporan):
    # selisih = waktu_pelaporan - timezone.now()
    selisih = timezone.now() - waktu_pelaporan
    if selisih < datetime.timedelta(days=config.MASA_BERLAKU_NS):
        return True
    else:
        return False
