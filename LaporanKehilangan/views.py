from django.views.generic import DetailView, TemplateView, UpdateView
from django.conf import settings
from django.http import Http404
from django.utils import timezone
from django.shortcuts import render, redirect, reverse
from django.shortcuts import get_object_or_404
from django.core.exceptions import ObjectDoesNotExist
from LaporanKehilanganUser.models import LaporanKehilanganUser
from LaporanKehilanganUser.forms import LaporanKehilanganUserForm
from LaporanKehilangan.models import LaporanKehilangan
from LaporanKehilanganUser.views import LaporanKehilanganUserDetail
from LaporanKehilanganUser.views import  LaporanKehilanganUserUpdate
from InitData.models import Agama, Kelamin, Kebangsaan, Pekerjaan, Lokasi
from core.templatetags.core_tags import nomorsuratLaporanKehilangan
from core.templatetags.core_filters import roman_number


class Index(TemplateView):
    template_name = 'LaporanKehilangan/index.html'


def getLaporanKehilanganUserDetail(request):
    template_name = "LaporanKehilangan/user_status_form.html"
    if request.POST and request.POST['qrcode']:
        qrcode = request.POST['qrcode']
        # if isinstance(qrcode, int):
        if qrcode.isdigit():
            try:
                qs = LaporanKehilanganUser.objects.get(
                    hash_field=qrcode, is_valid=True, is_expired=False)
                STATUS = True
            except ObjectDoesNotExist:
                raise Http404("Record is not found!")

            if qs.is_approved:
                try:
                    qs = LaporanKehilangan.objects.get(
                        hash_field=qrcode, is_valid=True, is_expired=False
                    )
                    STATUS = False
                    print(qs.is_approved)
                except ObjectDoesNotExist:
                    raise Http404("Record is not found!")

        else:
            try:
                qs = LaporanKehilanganUser.objects.get(
                    id=qrcode, is_valid=True, is_expired=False)
                STATUS = True
            except ObjectDoesNotExist:
                raise Http404("Record is not found!")

            if qs.is_approved:
                try:
                    qs = LaporanKehilangan.objects.get(
                        uuid=qrcode, is_valid=True, is_expired=False
                    )
                    STATUS = False
                except ObjectDoesNotExist:
                    raise Http404("Record is not found!")

        if STATUS:
            # Check if the report is_approved = False
            # If is_approved = True, disable "Sunting" Button and change with Ready to Print
            return redirect('laporankehilangan:laporankehilanganuser-detail', pk = qs.id)
        else:
            return redirect(reverse('laporankehilangan:laporankehilangan-print', args = {qs.id}))

    else:
        raise Http404("Record is not found!")


# class LaporanKehilanganUserDetail(DetailView):
# inherit from LaporanKehilanganUser.views
class LaporanKehilanganUserDetail(LaporanKehilanganUserDetail):
    model = LaporanKehilanganUser
    # form_class = LaporanKehilanganUserForm
    template_name = 'LaporanKehilangan/user_status_form.html'

    # This is to prevent direct call the link without referer
    '''
    def get(self, request, *args, **kwargs):
        if self.request.META.get('HTTP_REFERER') or settings.DEBUG:
            print(self.request.META.get('HTTP_REFERER'))
            self.object = self.get_object()
            context = super(LaporanKehilanganUserDetail,
                            self).get_context_data(*args, **kwargs)
            return self.render_to_response(context=context)
        else:
            raise Http404("Record is not found!")
    '''

# class LaporanKehilanganUserUpdate(UpdateView):
class LaporanKehilanganUserUpdate(LaporanKehilanganUserUpdate):
    model = LaporanKehilanganUser
    form_class = LaporanKehilanganUserForm
    template_name = 'LaporanKehilangan/user_edit_form.html'
    # success_url = reverse('laporankehilangan:laporankehilanganuser-detail')
    
    # This is to prevent direct call the link without referer
    '''
    def get(self, request, *args, **kwargs):
        if self.request.META.get('HTTP_REFERER'):
            # print(self.request.META.get('HTTP_REFERER'))
            self.object = self.get_object()
            context = super(LaporanKehilanganUserUpdate,
                            self).get_context_data(*args, **kwargs)
            return self.render_to_response(context=context)
        else:
            raise Http404("Record is not found!")
    '''

    def get_success_url(self, *args, **kwargs):
        return reverse('laporankehilangan:laporankehilanganuser-detail', args = {self.object.id})


# EDIT THIS FOR FURTHER PROCESSING
def LaporanKehilanganContinue(request, pk):
    if request.META.get('HTTP_REFERER'):
        # print(request.META.get('HTTP_REFERER'))
        # qs = LaporanKehilanganUser.objects.filter(id=pk)
        # if qs.count() > 0:

        try:
            qs = LaporanKehilanganUser.objects.get(id=pk, is_valid=True)

            qs.is_valid = True
            qs.save()

        except ObjectDoesNotExist:
            raise Http404("Record is not found!")
        
        lokasi = Lokasi.objects.all()

        return render(request, 'LaporanKehilangan/continue_process.html',
                      {
                          'pk': pk,
                          'hf': qs.hash_field,
                          'object': qs,
                          'lokasi': lokasi,
                      })
        # else:
        #    raise Http404("Record is not found!")
    else:
        raise Http404("Record is not found!")


        # EDIT THIS FOR FURTHER PROCESSING
def LaporanKehilanganSave(request):
    if request.META.get('HTTP_REFERER') or settings.DEBUG:

        agama = request.POST.get('id_agama')
        kelamin = request.POST.get('id_kelamin')
        pekerjaan = request.POST.get('id_pekerjaan')
        kebangsaan = request.POST.get('id_kebangsaan')
               
        qs = LaporanKehilangan()

        # Fields Foreignkey
        qs.agama = Agama.objects.get(jenis_agama=agama)
        qs.kelamin = Kelamin.objects.get(jenis_kelamin=kelamin)
        qs.pekerjaan = Pekerjaan.objects.get(jenis_pekerjaan=pekerjaan)
        qs.kebangsaan = Kebangsaan.objects.get(jenis_kebangsaan=kebangsaan)

        # Fields User
        qs.nama = request.POST.get('id_nama')
        qs.tempat_lahir = request.POST.get('id_tempat_lahir')
        qs.tanggal_lahir = request.POST.get('id_tanggal_lahir')
        qs.alamat = request.POST.get('id_alamat')
        qs.kota = request.POST.get('id_kota')
        qs.nomor_telpon = request.POST.get('id_nomor_telpon')
        qs.email = request.POST.get('id_email')
        qs.keterangan_kehilangan = request.POST.get('id_keterangan_kehilangan')
        qs.lokasi_kehilangan = request.POST.get('id_lokasi_kehilangan')
        qs.jenis_barang_hilang = request.POST.get('id_jenis_barang_hilang')
        qs.tanggal_kehilangan = request.POST.get('id_tanggal_kehilangan')
        
        # Field Back Office
        qs.jabatan = request.POST.get('id_jabatan')
        qs.pembuat_laporan = request.POST.get('id_pembuat_laporan')
        qs.pangkat_pembuat_laporan = request.POST.get('id_pangkat_pembuat_laporan')
        qs.lokasi_pelaporan = request.POST.get('id_lokasi_pelaporan')
        
        # Fields Control
        qs.uuid = request.POST.get('id_uuid')
        qs.hash_field = request.POST.get('id_hash_field')
        qs.is_valid = True
        qs.is_approved = True

        # Nomor Surat
        qs.nomor_surat = nomorsuratLaporanKehilangan()
        qs.nomor_surat_text = '%s / %s / %s / %s / %s / %s' % (
            qs.nomor_surat,
            'B',
            roman_number(timezone.now().month),
            timezone.now().year,
            qs.lokasi_pelaporan,
            'SPKT'
            )
            
        # qs.save()

        # Save into LaporanKehilangan    
        try:
            qs.save()
        except Exception:
            # from django.db import connection
            # print(connection.queries[-1])
            # print ('%s (%s)' % (e.message, type(e)))
            raise Http404("Something Wrong! Can't Save LK!")
        
        # Update LaporanKehilanganUser. is_approved=True
        id_uuid = request.POST.get('id_uuid')
        
        try:
            qs_user = LaporanKehilanganUser.objects.get(id=id_uuid)
        except ObjectDoesNotExist:
            raise Http404("Record is not found!")

        qs_user.is_approved = True;
        qs_user.is_valid = True;

        # Try to save into LaporanKehilanganUser
        try:
            qs_user.save()
        except Exception:
            raise Http404("Something Wrong! Can't Save!")

        # Get The Lastest record and 
        # try:
        #    qs = LaporanKehilangan.objects.get(uuid=id_uuid, is_valid=True, is_approved=True)

        # except ObjectDoesNotExist:
        #    raise Http404("Record is not found!")

        # return LaporanKehilanganPrint.as_view()(request, pk=qs.id)
        return redirect(reverse('laporankehilangan:laporankehilangan-print', args = {qs.id}))

    else:
        raise Http404("Record is not found!")


class LaporanKehilanganPrint(DetailView):
    model = LaporanKehilangan
    template_name = 'LaporanKehilangan/save_and_print.html'

    def get(self, request, *args, **kwargs):
        print(self.request.META.get('HTTP_REFERER'))
        print(settings.DEBUG)
        if self.request.META.get('HTTP_REFERER') or settings.DEBUG:
            self.object = self.get_object()
            context = super(LaporanKehilanganPrint,
                            self).get_context_data(*args, **kwargs)
            return self.render_to_response(context=context)
        else:
            raise Http404("Record is not found!")
