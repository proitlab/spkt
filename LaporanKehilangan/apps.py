from django.apps import AppConfig


class LaporankehilanganConfig(AppConfig):
    name = 'LaporanKehilangan'
    verbose_name = 'Surat Laporan Kehilangan'
