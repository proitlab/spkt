from django.urls import path
from LaporanKehilangan.views import Index, getLaporanKehilanganUserDetail
from LaporanKehilangan.views import LaporanKehilanganUserDetail, LaporanKehilanganUserUpdate
from LaporanKehilangan.views import LaporanKehilanganContinue, LaporanKehilanganSave
from LaporanKehilangan.views import LaporanKehilanganPrint

app_name = 'laporankehilangan'
urlpatterns = [
    path('', Index.as_view(), name='index'),
    path(
        'g/',
        getLaporanKehilanganUserDetail,
        name='getlaporankehilanganuser-detail'),
    path(
        'd/<uuid:pk>/',
        LaporanKehilanganUserDetail.as_view(),
        name='laporankehilanganuser-detail'),
    path(
        'u/<uuid:pk>/',
        LaporanKehilanganUserUpdate.as_view(),
        name='laporankehilanganuser-update'),
    #path(
    #    'e/<int:hf>/',
    #    LaporanKehilanganUserDetail.as_view(),
    #    name='laporankehilanganuser-detail-hf'),
    path(
        # 'q/<uuid:pk>/<int:hf>/',
        'q/<uuid:pk>/',
        LaporanKehilanganContinue,
        name='laporankehilangan-continue'),
    path(
        # 'q/<uuid:pk>/<int:hf>/',
        # 'p/<uuid:pk>/',
        'p/',
        LaporanKehilanganSave,
        name='laporankehilangan-save'),
    path(
        'print/<pk>/',
        LaporanKehilanganPrint.as_view(),
        name='laporankehilangan-print'),        
]
