# Generated by Django 2.0.4 on 2018-04-17 07:28

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone
import uuid


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('InitData', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='LaporanKehilangan',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uuid', models.UUIDField(default=uuid.uuid4, editable=False, unique=True)),
                ('nama', models.CharField(max_length=50)),
                ('tempat_lahir', models.CharField(default=None, max_length=20)),
                ('tanggal_lahir', models.DateField(default=None)),
                ('alamat', models.CharField(max_length=200)),
                ('kota', models.CharField(max_length=20)),
                ('nomor_telpon', models.CharField(max_length=20)),
                ('email', models.EmailField(blank=True, max_length=254)),
                ('jenis_barang_hilang', models.CharField(max_length=100)),
                ('tanggal_kehilangan', models.DateField(blank=True, default=django.utils.timezone.now)),
                ('lokasi_kehilangan', models.CharField(blank=True, max_length=50)),
                ('keterangan_kehilangan', models.TextField(blank=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('hash_field', models.PositiveIntegerField(default=0)),
                ('is_valid', models.BooleanField(default=False)),
                ('is_approved', models.BooleanField(default=False)),
                ('is_expired', models.BooleanField(default=False)),
                ('nomor_surat', models.CharField(max_length=50)),
                ('waktu_pelaporan', models.DateTimeField(auto_now_add=True)),
                ('lokasi_pelaporan', models.CharField(max_length=50)),
                ('pembuat_laporan', models.CharField(blank=True, max_length=30)),
                ('jabatan', models.CharField(max_length=20)),
                ('pangkat_pembuat_laporan', models.CharField(blank=True, max_length=50)),
                ('agama', models.ForeignKey(default=1, on_delete=django.db.models.deletion.DO_NOTHING, to='InitData.Agama')),
                ('created_by', models.ForeignKey(default=1, on_delete=django.db.models.deletion.DO_NOTHING, to=settings.AUTH_USER_MODEL)),
                ('kebangsaan', models.ForeignKey(default=1, on_delete=django.db.models.deletion.DO_NOTHING, to='InitData.Kebangsaan')),
                ('kelamin', models.ForeignKey(default=1, on_delete=django.db.models.deletion.DO_NOTHING, to='InitData.Kelamin')),
                ('pekerjaan', models.ForeignKey(default=1, on_delete=django.db.models.deletion.DO_NOTHING, to='InitData.Pekerjaan')),
            ],
        ),
    ]
