# from django.conf.urls import url
from django.url import path
# from django.conf.urls import include
# from django.contrib import admin
from django.contrib.auth import views as auth_views
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('login/', auth_views.login, name='login'),
    path(
        'logout/', auth_views.logout,
        {'next_page': '/sps/login/'},
        name='logout'),
    path('dashboard/', views.dashboard, name='dashboard'),
    path('accounts/', views.dashboard, name='dashboard'),
    path('nomorsurat/baru/', views.nomorsurat_baru, name='nomorsurat-baru'),
    # url(r'^nomorsurat/ubah/(?P<pk>\d+)$',
    #       views.nomorsurat_ubah, name='nomorsurat-ubah'),
    path(
        'nomorsurat/list/',
        views.LaporanKehilanganListView.as_view(),
        name='nomorsurat-list'),
    path(
        'nomorsurat/detail/<int:pk>',
        views.LaporanKehilanganDetailView.as_view(),
        name='nomorsurat-detail'),
    path(
        'nomorsurat/detail/<int:pk>/print',
        views.LaporanKehilanganDetailPrint.as_view(),
        name='nomorsurat-print'),
    path(
        'nomorsurat/detail/<int:pk>/edit',
        views.nomorsurat_edit,
        name='nomorsurat-edit'),
    # url(r'^nomorsurat/sunting/(?P<pk>\d+)$',
    #   views.NomorSuratUpdateView.as_view(), name='nomorsurat-sunting'),
    # url(r'^.*$', views.index, name='index'),
    #    url(r'^tinymce/', include('tinymce.urls')),
]
