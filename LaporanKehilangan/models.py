import uuid
from django.contrib.auth.models import User
from django.db import models
from django.urls import reverse
from django.utils import timezone
from tinymce import models as tinymce_models
from InitData.models import KepalaSiaga
from InitData.models import Kelamin, Agama, Pekerjaan, Kebangsaan
from core.models import UpperCaseCharField, UpperCaseCharWithoutSpaceField, UpperCaseEmailField

# from core.templatetags.core_tags import nomorsuratLaporanKehilangan


class LaporanKehilangan(models.Model):
    # Main Fields
    uuid = models.UUIDField(unique=True, default=uuid.uuid4, editable=False)
    nama = UpperCaseCharField(max_length=50)
    tempat_lahir = UpperCaseCharField(max_length=20, default=None)
    tanggal_lahir = models.DateField(
        auto_now=False, auto_now_add=False, default=None)
    kelamin = models.ForeignKey(
        Kelamin, default=1, on_delete=models.DO_NOTHING)
    agama = models.ForeignKey(Agama, default=1, on_delete=models.DO_NOTHING)
    pekerjaan = models.ForeignKey(
        Pekerjaan, default=1, on_delete=models.DO_NOTHING)
    kebangsaan = models.ForeignKey(
        Kebangsaan, default=1, on_delete=models.DO_NOTHING)
    alamat = UpperCaseCharField(max_length=200)
    kota = UpperCaseCharField(max_length=20)
    nomor_telpon = UpperCaseCharField(max_length=20)
    email = UpperCaseEmailField(blank=True)
    jenis_barang_hilang = UpperCaseCharField(max_length=100)
    tanggal_kehilangan = models.DateField(
        auto_now=False, auto_now_add=False, blank=True, default=timezone.now)
    lokasi_kehilangan = UpperCaseCharField(max_length=50, blank=True)
    keterangan_kehilangan = models.TextField(blank=True)
    # keterangan_kehilangan = tinymce_models.HTMLField(blank=True)

    # Some more fields
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    hash_field = models.PositiveIntegerField(default=0)
    is_valid = models.BooleanField(default=False)
    is_approved = models.BooleanField(default=False)
    is_expired = models.BooleanField(default=False)

    # Nomor Surat
    nomor_surat = models.PositiveIntegerField(default=0)
    nomor_surat_text = UpperCaseCharField(max_length=50, blank=True)
    waktu_pelaporan = models.DateTimeField(auto_now=False, auto_now_add=True)
    lokasi_pelaporan = UpperCaseCharWithoutSpaceField(max_length=50)

    # Tanda Tangan
    pembuat_laporan = UpperCaseCharField(max_length=30, blank=True)
    # jabatan = models.ForeignKey(
    #    KepalaSiaga, default=1, on_delete=models.DO_NOTHING)
    jabatan = UpperCaseCharField(max_length=20)
    pangkat_pembuat_laporan = UpperCaseCharField(max_length=50, blank=True)

    created_by = models.ForeignKey(
        User, on_delete=models.DO_NOTHING, default=1)

    def __str__(self):
        return '%s' % (self.nomor_surat_text)

    def get_absolute_url(self):
        return reverse('laporankehilangan-detail', kwargs={'pk': self.pk})


'''
    def save(self):
        self.full_clean()
        nomor_surat = nomorsuratLaporanKehilangan()
        nomor

        super(LaporanKehilangan, self).save(*args, **kwargs)
'''