from django.forms.extras.widgets import SelectDateWidget
from django.forms import widgets
from django.forms import ModelForm
from .models import LaporanKehilangan
from tinymce.widgets import TinyMCE
# from django.contrib.auth.models import User

from .utils import buat_nomorsurat

from django import forms

# from constance import config


class LaporanKehilanganUpdateForm(ModelForm):
    class Meta:
        model = LaporanKehilangan
        # fields = ['nomor_surat', 'lokasi_pelaporan',
        fields = [
            'nama',
            'tempat_lahir',
            'tanggal_lahir',
            'kelamin',
            'agama',
            'pekerjaan',
            'kebangsaan',
            'alamat',
            'kota',
            'nomor_telpon',
            'email',
            'jenis_barang_hilang',
            'tanggal_kehilangan',
            'lokasi_kehilangan',
            'keterangan_kehilangan',
            'pembuat_laporan',
            'jabatan',
            'pangkat_pembuat_laporan',
        ]

        widgets = {
            'nomor_surat':
            widgets.TextInput(attrs={
                'class': 'vTextField',
                'readonly': 'readonly'
            }),
            'tanggal_lahir':
            SelectDateWidget(years=range(2010, 1920, -1)),
            # 'tanggal_lahir': widgets.TextInput(
            # attrs={'class': 'vDateField'} ),
            'email':
            widgets.EmailInput(),
            'nama':
            widgets.TextInput(attrs={'class': 'vTextField'}),
            'tempat_lahir':
            widgets.TextInput(attrs={'class': 'vTextField'}),
            'kota':
            widgets.TextInput(attrs={'class': 'vTextField'}),
            'nomor_telpon':
            widgets.TextInput(attrs={'class': 'vTextField'}),
            # 'email':
            # widgets.TextInput(attrs={'class': 'vTextField'}),
            'alamat':
            widgets.TextInput(attrs={'class': 'vTextField'}),
            'jenis_barang_hilang':
            widgets.TextInput(attrs={'class': 'vTextField'}),
            'lokasi_pelaporan':
            widgets.TextInput(attrs={
                'class': 'vTextField',
                'readonly': 'readonly'
            }),
            'kelamin':
            widgets.RadioSelect,
            'kebangsaan':
            widgets.RadioSelect,
            'tanggal_kehilangan':
            SelectDateWidget(),
            'lokasi_kehilangan':
            widgets.TextInput(attrs={'class': 'vTextField'}),
            'keterangan_kehilangan':
            TinyMCE(attrs={
                'cols': 200,
                'rows': 80
            }),
            'pembuat_laporan':
            widgets.TextInput(attrs={'class': 'vTextField'}),
            'pangkat_pembuat_laporan':
            widgets.TextInput(attrs={'class': 'vTextField'}),
            # 'alamat': widgets.Textarea(),
            # 'tanggal_lahir': DateInput()
        }


class LaporanKehilanganForm(ModelForm):
    class Meta:
        model = LaporanKehilangan
        fields = [
            'lokasi_pelaporan',
            'nama',
            'tempat_lahir',
            'tanggal_lahir',
            'kelamin',
            'agama',
            'pekerjaan',
            'kebangsaan',
            'alamat',
            'kota',
            # 'agama', 'pekerjaan', 'kebangsaan', 'alamat', 'kota',
            'nomor_telpon',
            'email',
            'jenis_barang_hilang',
            'tanggal_kehilangan',
            'lokasi_kehilangan',
            'keterangan_kehilangan',
            'pembuat_laporan',
            'jabatan',
            'pangkat_pembuat_laporan',
        ]
        widgets = {
            'tanggal_lahir':
            SelectDateWidget(years=range(2010, 1920, -1)),
            # 'tanggal_lahir': widgets.TextInput(
            # attrs={'class': 'vDateField'} ),
            'email':
            widgets.EmailInput(),
            'nama':
            widgets.TextInput(attrs={'class': 'vTextField'}),
            'tempat_lahir':
            widgets.TextInput(attrs={'class': 'vTextField'}),
            'kota':
            widgets.TextInput(attrs={'class': 'vTextField'}),
            'nomor_telpon':
            widgets.TextInput(attrs={'class': 'vTextField'}),
            # 'email':
            # widgets.TextInput(attrs={'class': 'vTextField'}),
            'alamat':
            widgets.TextInput(attrs={'class': 'vTextField'}),
            'jenis_barang_hilang':
            widgets.TextInput(attrs={'class': 'vTextField'}),
            'lokasi_pelaporan':
            widgets.HiddenInput,
            'kelamin':
            widgets.RadioSelect,
            'kebangsaan':
            widgets.RadioSelect,
            'tanggal_kehilangan':
            SelectDateWidget(),
            'lokasi_kehilangan':
            widgets.TextInput(attrs={'class': 'vTextField'}),
            'keterangan_kehilangan':
            TinyMCE(attrs={
                'cols': 200,
                'rows': 80
            }),
            'pembuat_laporan':
            widgets.TextInput(attrs={'class': 'vTextField'}),
            'pangkat_pembuat_laporan':
            widgets.TextInput(attrs={'class': 'vTextField'}),
            # 'alamat': widgets.Textarea(),
            # 'tanggal_lahir': DateInput()
        }

    def save(self, commit=True):
        ns = super(LaporanKehilanganForm, self).save(commit=False)
        # awal_ns = int(Konfigurasi.objects.get())
        # ns_total = NomorSurat.objects.count()

        ns.nomor_surat = buat_nomorsurat(ns.lokasi_pelaporan)

        # ns.lokasi_pelaporan = 'SPKT'
        # ns.lokasi_pelaporan = request

        ns.save()

        return ns


class AgamaForm(forms.Form):
    jenis_agama = forms.CharField(label='Agama', max_length=20)
