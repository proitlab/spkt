$(function() {
  $( "#id_tanggal_lahir" ).datepicker({
      dateFormat: 'dd-mm-yy',
      yearRange: "-100:+0", });
});

$(function() {
  $( "#id_tanggal_kehilangan" ).datepicker({
      dateFormat: 'dd-mm-yy', });
});

$(function() {
  $( "#id_waktu_kejadian" ).datetimepicker({
    dateFormat: 'dd-mm-yy',
    controlType: 'select',
  });
});


new Vue({
  el : '#pernyataan',
  data: {
    checked: false
  }
});

var vm = new Vue({
  el: '#vue_pembuat_laporan',
  data: {
    vue_lokasi_pelaporan: 'PMJ',
    vue_jabatan: '',
    vue_nama_pembuat_laporan: '',
    vue_pangkat_pembuat_laporan: '',
    vue_checked: false
  },
  delimiters: ['[[',']]'],
});

// vm.$watch('vue_pembuat_laporan', function(value) {
//    $('[id^="pembuat_laporan"]').val(value);
// });