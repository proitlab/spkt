from django.db import models

# Class to Make UpperCase Charfield


class UpperCaseEmailField(models.EmailField):
    def __init__(self, *args, **kwargs):
        super(UpperCaseEmailField, self).__init__(*args, **kwargs)

    def pre_save(self, model_instance, add):
        value = getattr(model_instance, self.attname, None)
        if value:
            value = value.upper()
            setattr(model_instance, self.attname, value)
            return value
        else:
            return super(UpperCaseEmailField, self).pre_save(
                model_instance, add)


class UpperCaseCharField(models.CharField):
    def __init__(self, *args, **kwargs):
        super(UpperCaseCharField, self).__init__(*args, **kwargs)

    def pre_save(self, model_instance, add):
        value = getattr(model_instance, self.attname, None)
        if value:
            value = value.upper()
            setattr(model_instance, self.attname, value)
            return value
        else:
            return super(UpperCaseCharField, self).pre_save(
                model_instance, add)


class UpperCaseCharWithoutSpaceField(UpperCaseCharField):
    def __init__(self, *args, **kwargs):
        super(UpperCaseCharWithoutSpaceField, self).__init__(*args, **kwargs)

    def pre_save(self, model_instance, add):
        value = getattr(model_instance, self.attname, None)
        if value:
            value = value.replace(" ", "")
            setattr(model_instance, self.attname, value)
            return value
        else:
            return super(UpperCaseCharWithoutSpaceField, self).pre_save(
                model_instance, add)
