from django.template import Library, TemplateSyntaxError
from constance import config
from LaporanKehilangan.models import LaporanKehilangan
from LaporanKasus.models import LaporanKasus
from LaporanPolisi.models import LaporanPolisi

register = Library()

@register.simple_tag
def nomorsuratLaporanKehilangan():
    try:
        latest_record = LaporanKehilangan.objects.last()
        current_ns = latest_record.nomor_surat
        current_ns += 1
    except Exception:
        current_ns = config.AWAL_NS_LAPORAN_KEHILANGAN
    
    return current_ns

@register.simple_tag
def nomorsuratLaporanKasus():
    try:
        latest_record = LaporanKasus.objects.last()
        current_ns = latest_record.nomor_surat
        current_ns += 1
    except Exception:
        current_ns = config.AWAL_NS_LAPORAN_KASUS

    return current_ns

@register.simple_tag
def nomorsuratLaporanPolisi():
    try:
        latest_record = LaporanPolis.objects.last()
        current_ns = latest_record.nomor_surat
        current_ns += 1
    except Exception:
        current_ns = config.AWAL_NS_LAPORAN_POLISI

    return current_ns