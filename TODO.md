#Todo
- Check validity of Laporan Kehilangan. Jika tidak di is_approved=TRUE dalam 24x2 (Configurable) jam (example), set is_expired=True -> Use Celery
- is_approved -> Jika di approve di SPKT
- is_valid -> Jika di submit ke system "Kirim". Default=False
- Demikian pula dengan UpdateView -> will show Http404, but need to check is_valid
- Create custom Http404


#Done
- QR check into database kalau ada tidak record. Jika tidak ada, tampilkan error. -> show Http404 Error


***NOTE***
- Get FULL Url
***Template***
    {{ request.build_absolute_uri }}{{ object.get_absolute_url }}
    
    {{request.build_absolute_uri}}{{object.get_absolute_url|slice:"1:"}}
    
    {% if request.is_secure %}https://{% else %}http://{% endif %}{{ request.get_host }}{{ object.get_absolute_url }} because {{ request.build_absolute_uri }}
***View***
    request.build_absolute_uri(reverse('view_name', args=(obj.pk, )))